variable "subscription_id" {
  description = "The subscription ID where resources will be created"
}

variable "tenant_id" {
  description = "The tenant ID where resources will be created"
}

variable "location" {
  description = "The Azure region to deploy resources"
  default     = "Germany West Central"
}